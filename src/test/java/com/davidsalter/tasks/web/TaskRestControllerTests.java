/*
 * Copyright 2016 David Salter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.davidsalter.tasks.web;

import com.davidsalter.tasks.data.TaskRepository;
import com.davidsalter.tasks.domain.Task;
import com.davidsalter.tasks.domain.TaskStatistics;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:test.application.properties")
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:setup.sql"),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:tearDown.sql")})
public class TaskRestControllerTests {

    private static final String STATISTICS_URL = "/api/v1/tasks/statistics";
    private static final String FINDALL_URL = "/api/v1/tasks";
    private static final String CREATE_URL = "/api/v1/tasks";
    private static final String TASK_2_URL = "/api/v1/tasks/2";
    private static final String TASK_3_URL = "/api/v1/tasks/3";

    private static final String DESCRIPTION = "A description";
    private static final String NEW_DESCRIPTION = "A new description";

    private MockMvc mockMvc;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void setup() {

        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void shouldBeAbleToReadASingleTask() {

        ResponseEntity<Task> response = restTemplate.getForEntity(TASK_2_URL, Task.class);

        Task task = response.getBody();

        assertThat(task.getId(), is(equalTo(2L)));
        assertThat(task.getDescription(), is(equalTo(DESCRIPTION)));
    }

    @Test
    public void shouldBeAbleToCreateATask() {

        Task task = new Task();

        task.setDescription(NEW_DESCRIPTION);
        ResponseEntity<Task> response = restTemplate.postForEntity(CREATE_URL, task, Task.class);

        Task newTask = response.getBody();

        assertThat(newTask.getId(), is(notNullValue()));
        assertThat(newTask.getDescription(), is(equalTo(NEW_DESCRIPTION)));
    }

    @Test
    public void shouldBeAbleToReadAllTasks() {

        ResponseEntity<List> response = restTemplate.getForEntity(FINDALL_URL, List.class);

        List<Task> tasks = response.getBody();

        assertThat(tasks.size(), is(equalTo(3)));
    }

    @Test
    public void shouldBeAbleToDeleteASingleTask() {

        restTemplate.delete(TASK_3_URL);

        ResponseEntity<Task> response = restTemplate.getForEntity(TASK_3_URL, Task.class);

        Task task = response.getBody();

        assertThat(task, is(nullValue()));
    }

    @Test
    public void shouldBeAbleToRetrieveStatisticsAsJson() throws Exception {
        mockMvc.perform(get(STATISTICS_URL).accept(MediaType.parseMediaType("application/json")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType));
    }

    @Test
    public void shouldBeAbleToRetrieveValidStatistics() throws Exception {
        ResponseEntity<TaskStatistics> response =
                restTemplate.getForEntity(STATISTICS_URL, TaskStatistics.class);

        TaskStatistics statistics = response.getBody();

        assertThat(statistics.getOpenTasks(), is(equalTo(3L)));
        assertThat(statistics.getOverdue(), is(equalTo(1L)));
        assertThat(statistics.getCreatedToday(), is(equalTo(3L)));
        assertThat(statistics.getDueToday(), is(equalTo(1L)));
    }
}
