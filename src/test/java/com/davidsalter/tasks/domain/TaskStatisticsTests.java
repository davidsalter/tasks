/*
 * Copyright 2016 David Salter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.davidsalter.tasks.domain;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class TaskStatisticsTests {

    private static final Long CREATED_TODAY = 99L;
    private static final Long DUE_TODAY = 88L;
    private static final Long OPEN_TASKS = 111L;
    private static final Long OVERDUE = 201L;

    private TaskStatistics taskStatistics = new TaskStatistics();

    @Before
    public void setup() {

        taskStatistics.setCreatedToday(CREATED_TODAY);
        taskStatistics.setDueToday(DUE_TODAY);
        taskStatistics.setOpenTasks(OPEN_TASKS);
        taskStatistics.setOverdue(OVERDUE);
    }

    @Test
    public void shouldBeAbleToAccessCreatedTodayCount() {
        assertThat(CREATED_TODAY, is(equalTo(taskStatistics.getCreatedToday())));
    }

    @Test
    public void shouldBeAbleToAccessDueCount() {
        assertThat(DUE_TODAY, is(equalTo(taskStatistics.getDueToday())));
    }

    @Test
    public void shouldBeAbleToAccessOpenCount() {
        assertThat(OPEN_TASKS, is(equalTo(taskStatistics.getOpenTasks())));
    }

    @Test
    public void shouldBeAbleToAccessOverdueCount() {
        assertThat(OVERDUE, is(equalTo(taskStatistics.getOverdue())));
    }
}
