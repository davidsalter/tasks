/*
 * Copyright 2016 David Salter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.davidsalter.tasks.domain;


import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;


public class TaskTests {

    private static final Long ID = 99L;
    private static final String NAME = "My Name";
    private static final String STATUS = "OPEN";
    private Task task = new Task();
    private LocalDate today = LocalDate.now();

    private static final String DESCRIPTION = "This is a task";

    @Before
    public void setup() {
        task.setCreated(java.sql.Date.valueOf(today));
        task.setDescription(DESCRIPTION);
        task.setDueDate(java.sql.Date.valueOf(today));
        task.setId(ID);
        task.setName(NAME);
        task.setStatus(STATUS);
    }

    @Test
    public void shouldBeBeAbleToAccessCreatedDate() {
        assertThat(java.sql.Date.valueOf(today), is(equalTo(task.getCreated())));
    }

    @Test
    public void shouldBeBeAbleToAccessDescription() {
        assertThat(DESCRIPTION, is(equalTo(task.getDescription())));
    }

    @Test
    public void shouldBeBeAbleToAccessDueDate() {
        assertThat(java.sql.Date.valueOf(today), is(equalTo(task.getDueDate())));
    }

    @Test
    public void shouldBeBeAbleToAccessId() {
        assertThat(ID, is(equalTo(task.getId())));
    }

    @Test
    public void shouldBeBeAbleToAccessName() {
        assertThat(NAME, is(equalTo(task.getName())));
    }

    @Test
    public void shouldBeBeAbleToAccessStatus() {
        assertThat(STATUS, is(equalTo(task.getStatus())));
    }
}
