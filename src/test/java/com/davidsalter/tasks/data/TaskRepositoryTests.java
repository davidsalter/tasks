/*
 * Copyright 2016 David Salter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.davidsalter.tasks.data;

import com.davidsalter.tasks.domain.Task;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@TestPropertySource(locations = "classpath:test.application.properties")
public class TaskRepositoryTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TaskRepository taskRepository;

    @Before
    public void setup() {
        LocalDate today = LocalDate.now();
        LocalDate yesterday = today.minusDays(1);
        LocalDate lastWeek = today.minusDays(1);

        Task createdToday = new Task();
        createdToday.setCreated(java.sql.Date.valueOf(today));
        createdToday.setDueDate(java.sql.Date.valueOf(today));
        this.entityManager.persist(createdToday);

        Task createdYesterday = new Task();
        createdYesterday.setCreated(java.sql.Date.valueOf(yesterday));
        createdYesterday.setDueDate(java.sql.Date.valueOf(yesterday));
        this.entityManager.persist(createdYesterday);

        Task createdLastWeek = new Task();
        createdLastWeek.setCreated(java.sql.Date.valueOf(lastWeek));
        createdLastWeek.setDueDate(java.sql.Date.valueOf(yesterday));
        this.entityManager.persist(createdLastWeek);
    }

    @Test
    public void shouldReturnCountByCreated() {
        assertThat(taskRepository.countByCreated()).isEqualTo(1);
    }

    @Test
    public void shouldReturnCountByOverdue() {
        assertThat(taskRepository.countByOverdue()).isEqualTo(2);
    }

    @Test
    public void shouldReturnCountByDueToday() {
        assertThat(taskRepository.countByDueToday()).isEqualTo(1);
    }
}
