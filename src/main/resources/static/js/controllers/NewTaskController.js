(function () {
	'use strict';

	angular
		.module('tasksApp')
		.controller('NewTaskController', NewTaskController);

	function NewTaskController(TaskListService, $scope) {

		var vm = this;
		vm.newDescription='';
		vm.newDueDate = '';


		vm.addTask = function() {
			TaskListService.addTask(vm.newDescription, vm.newDueDate).success(function(tasks) {
				populateTasks();
				vm.newDescription = "";
				vm.newDueDate = "";
			});
		};
		
		function populateTasks() {
			$scope.$emit('taskAdded', '');
		}

	}

})();