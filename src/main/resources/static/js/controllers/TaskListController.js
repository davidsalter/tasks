(function() {
	'use strict';

	angular.module('tasksApp').controller('TaskListController',
			TaskListController);

	function TaskListController(TaskListService, TaskStatisticsService, $scope) {

		var vm = this;

		vm.newDescription = "";

		populateTasks();

		vm.deleteTask = function(id) {

			TaskListService.deleteTask(id).success(function(tasks) {
				populateTasks();
			});
		};

		function populateTasks() {
			TaskListService
					.getTasks()
					.success(
							function(tasks) {
								vm.tasks = tasks;
								var stats = TaskStatisticsService
										.getStatistics()
										.success(
												function(stats) {
													vm.openTasksCount = stats.openTasks;
													vm.tasksCreatedTodayCount = stats.createdToday;
													vm.tasksOverdueCount = stats.overdue;
													vm.tasksDueTodayCount = stats.dueToday;
												});

							});
		}

		$scope.$on('taskAdded', function(event, args) {
			populateTasks();
		});

	}

})();