(function () {
	'use strict';

	angular
		.module('tasksApp')
		.service('TaskListService', TaskListService);

	function TaskListService($http) {

		this.getTasks = function () {
			return $http.get('/api/v1/tasks/');
		};

		this.deleteTask = function (id) {
			return $http.delete('/api/v1/tasks/' + id);
		};

		this.addTask = function (name, dueDate) {
			return $http.post('/api/v1/tasks', {
				'name': name,
				'dueDate': dueDate
			});
		};
	}

})();