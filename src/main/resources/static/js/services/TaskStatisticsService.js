(function () {
	'use strict';

	angular
		.module('tasksApp')
		.service('TaskStatisticsService', TaskStatisticsService);

	function TaskStatisticsService($http) {

		this.getStatistics = function () {
			return $http.get('/api/v1/tasks/statistics');
		};

	}

})();