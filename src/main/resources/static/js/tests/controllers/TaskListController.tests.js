describe('TaskListController', function () {
	var scope, rootScope, controller, mockTaskListService, deferred;

	beforeEach(function () {
		module('todoApp');
	});

	beforeEach(inject(function ($rootScope, $controller, $q) {
		rootScope = $rootScope;
		scope = $rootScope.$new();

		mockTaskListService = {
			getTasks: function () {
				var retVal = [{
					description: 'My Description'
				}];
				return {
					success: function (fn) {
						fn(retVal);
					}
				};
			}
		};

		mockTaskStatisticsService = {
			getOpenTaskCount: function (tasks) {
				return 3;
			},
			getTasksCreatedTodayCount: function (tasks) {
				return 2;
			}
		};

		controller = $controller('TaskListController', {
			$scope: scope,
			TaskListService: mockTaskListService,
			TaskStatisticsService: mockTaskStatisticsService
		});



	}));

	it('should be registered', function () {
		expect(controller).toBeDefined();
	});

	it('should populate statistics upon instantiation', function () {
		expect(controller.openTasksCount).toBe(3);
		expect(controller.tasksCreatedTodayCount).toBe(2);
	});

	it('should populate tasks upon instantiation', function () {
		expect(controller.tasks).toBeDefined();
		expect(controller.tasks[0].description).toBe('My Description');
	});

	it('should respond to the taskAdded event', function () {
		controller.tasks = null;
		scope.$emit('taskAdded', '');
		expect(controller.tasks).toBeDefined();
		expect(controller.tasks[0].description).toBe('My Description');
	});
});