describe('Application todoApp', function() {
	var mainModule;
	
	beforeEach(function() {
		mainModule = angular.module('todoApp');
	});
	
	it ('should be registered', function() {
		expect(mainModule).toBeDefined();
	});
});