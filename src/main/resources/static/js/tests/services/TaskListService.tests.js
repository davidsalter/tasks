describe('TaskListService', function () {

	var service;

	beforeEach(function () {
		module('todoApp');

		inject(function (TaskListService) {
			service = TaskListService;
		});
	});

	describe('when querying tasks', function () {
		beforeEach(inject(function ($injector) {
			$httpBackend = $injector.get('$httpBackend');

			$httpBackend.when('GET', '/tasks/')
				.respond([{
					description: 'My Description'
				}, {
					description: 'My 2nd Description'
				}]);

		}));

		it('should perform a HTTP GET operation', function () {
			$httpBackend.expectGET('/tasks/');
			var result = service.getTasks();
			$httpBackend.flush();
		});

		it('should retrieve a list of tasks', function () {
			$httpBackend.expectGET('/tasks/');
			var result = service.getTasks();
			result.success(function (tasks) {
				expect(tasks.length).toBe(2);
				expect(tasks[0].description).toBe('My Description');
				expect(tasks[1].description).toBe('My 2nd Description');
			});
			$httpBackend.flush();
		});
	});

	describe('when deleting tasks', function () {
		beforeEach(inject(function ($injector) {
			$httpBackend = $injector.get('$httpBackend');

			$httpBackend.when('DELETE', '/tasks/1')
				.respond({
					description: 'My Deleted Task'
				});

		}));

		it('should perform a HTTP DELETE operation', function () {
			$httpBackend.expectDELETE('/tasks/1');
			var result = service.deleteTask(1);
			$httpBackend.flush();
		});

		it('should return the deleted task', function () {
			$httpBackend.expectDELETE('/tasks/1');
			var result = service.deleteTask(1);
			result.success(function (task) {
				expect(task.description).toBe('My Deleted Task');
			});
			$httpBackend.flush();
		});

	});

	describe('when adding tasks', function () {
		beforeEach(inject(function ($injector) {
			$httpBackend = $injector.get('$httpBackend');

			$httpBackend.when('POST', '/tasks')
				.respond({
					description: 'My New Task'
				});

		}));
		it('should perform a HTTP POST operation', function () {
			$httpBackend.expectPOST('/tasks');
			var result = service.addTask('My New Task');
			$httpBackend.flush();
		});

		it('should return the added task', function () {
			$httpBackend.expectPOST('/tasks');
			var result = service.addTask('My New Task');
			result.success(function (task) {
				expect(task.description).toBe('My New Task');
			});
			$httpBackend.flush();
		});
		
		it('should send the new document as the request body', function () {
			$httpBackend.expect('POST', '/tasks', '{"description":"My New Task"}');
			var result = service.addTask('My New Task');
			$httpBackend.flush();
		});
	});
});