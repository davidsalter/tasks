describe('TaskStatisticService', function () {

	var service;

	beforeEach(function () {
		module('todoApp');

		inject(function (TaskStatisticsService) {
			service = TaskStatisticsService;
		});
	});

	it('should calculate the correct number of open tasks', function () {
		var result;
		tasks = ["a", "b", "c"];

		result = service.getOpenTaskCount(tasks);
		expect(result).toBe(3);
	});

	it('should calculate the correct number of tasks created today', function () {
		var today = new Date();
		var yesterday = new Date();
		yesterday.setDate(yesterday.getDate()-1);
		
		tasks = [{"description":"a", "created":today},
				 {"description":"b", "created":today},
				 {"description":"c", "created":yesterday}];

		result = service.getTasksCreatedTodayCount(tasks);
		expect(result).toBe(2);
	});
});