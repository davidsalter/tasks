/*
 * Copyright 2016 David Salter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.davidsalter.tasks.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.davidsalter.tasks.domain.Task;

public interface TaskRepository extends JpaRepository<Task, Long> {

    @Query("SELECT COUNT(t) FROM Task t WHERE t.created = CURRENT_DATE")
    Long countByCreated();

    @Query("SELECT COUNT(t) FROM Task t WHERE t.dueDate < CURRENT_DATE")
    Long countByOverdue();

    @Query("SELECT COUNT(t) FROM Task t WHERE t.dueDate = CURRENT_DATE")
    Long countByDueToday();

}
