package com.davidsalter.tasks.web;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.davidsalter.tasks.data.TaskRepository;
import com.davidsalter.tasks.domain.Task;
import com.davidsalter.tasks.domain.TaskStatistics;

@RestController
public class TaskRestController {

	@Autowired
	TaskRepository taskRepository;
	
	@RequestMapping(method=RequestMethod.GET, value="/api/v1/tasks/statistics")
	public TaskStatistics statistics() {
	
		TaskStatistics taskStatistics = new TaskStatistics();
		taskStatistics.setOpenTasks(taskRepository.count());
		taskStatistics.setCreatedToday(taskRepository.countByCreated());
		taskStatistics.setOverdue(taskRepository.countByOverdue());
		taskStatistics.setDueToday(taskRepository.countByDueToday());
		
		return taskStatistics;
		
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/api/v1/tasks/{taskId}")
	public Task findTask(@PathVariable("taskId") String taskId) {
	
		Task task = taskRepository.findOne(new Long(taskId));
		
		return task;
		
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/api/v1/tasks")
	public List<Task> findAllTask() {
	
		return taskRepository.findAll();

	}
	
	@RequestMapping(method=RequestMethod.POST, value="/api/v1/tasks")
	public Task createTask(@RequestBody Task newTask) {
	
		newTask.setCreated(new Date());
		Task task = taskRepository.save(newTask);
		
		return task;
		
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/api/v1/tasks/{taskId}")
	public String deleteTask(@PathVariable("taskId") String taskId) {
	
		taskRepository.delete(new Long(taskId));
		
		return taskId;
		
	}
	
}
